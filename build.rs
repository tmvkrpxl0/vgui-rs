use std::env;
use std::path::PathBuf;

fn main() {
    println!("cargo:rustc-link-search=./build");

    // Tell cargo to tell rustc to link the system bzip2
    // shared library.
    println!("cargo:rustc-link-lib=vgui");






    // The bindgen::Builder is the main entry point
    // to bindgen, and lets you build up options for
    // the resulting bindings.
    let bindings = bindgen::Builder::default()
        // The input header we would like to generate
        // bindings for.
        .header("cpp/bindings.h")
        .clang_args([
            "-x", "c++",
            "-Icpp/public",
            "-Icpp/public/tier0",
            "-Icpp/public/tier1",
            "-Icpp/public/bitmap",
            "-Wno-inconsistent-missing-override",
            "-D", "GNUC=1",
            "-D", "POSIX=1",
            "-D", "_LINUX=1",
            "-D", "LINUX=1",
        ])
        // Tell cargo to invalidate the built crate whenever any of the
        // included header files changed.
        .parse_callbacks(Box::new(bindgen::CargoCallbacks::new()))
        // Finish the builder and generate the bindings.
        .generate()
        // Unwrap the Result and panic on failure.
        .expect("Unable to generate bindings");

    // Write the bindings to the $OUT_DIR/bindings.rs file.
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");
}