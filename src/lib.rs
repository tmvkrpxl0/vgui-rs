mod vgui;
mod iapp_system;
mod key_value;
mod vector;

#[cfg(target_os = "windows")]
const IS_WINDOWS: bool = true;

#[cfg(not(target_os = "windows"))]
const IS_WINDOWS: bool = false;

#[cfg(target_os = "linux")]
const IS_LINUX: bool = true;

#[cfg(not(target_os = "linux"))]
const IS_LINUX: bool = false;

#[cfg(target_os = "macos")]
const IS_MACOS: bool = true;

#[cfg(not(target_os = "macos"))]
const IS_MACOS: bool = false;

const IS_POSIX: bool = IS_LINUX | IS_MACOS;
